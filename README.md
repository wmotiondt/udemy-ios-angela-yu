# Udemy-iOS-Angela-Yu

Program pengembangan minat dan bakat dalam pemrograman iOS dengan bahasa pemgrograman Swift 4 dan iOS 11

## Prerequisites
* macOS High Sierra 10 dan diatasnya
* Xcode 9
* iOS 11
* Logika Dasar Pemrograman

## Authors
* *Azmi Muhammad* - Technical Consultant di Wmotion

## Download File
Copas tulisan dibawah ini ke terminal/cmd

```
	git clone https://mazmik@bitbucket.org/wmotiondt/udemy-ios-angela-yu.git
```

## Material
* Dasar penempatan view (I Am Rich & I Am Poor Apps)
* Menghubungkan view dengan kodingan (Dicee, Destini, Quizzler & Magic 8 Ball Apps)
* Implementasi Auto Layout (Auto Layout Practice, & Auto Layout Calculator Apps)
* Konsumsi Open REST API (Clima & Bitcoin Ticker Apps)
* Implementasi Firebase (Flash Chat App)
* Implementasi penyimpanan lokal (CoreDataSimpleCRUD & Todo List App)
* Lainnya (Xylophone App)