import UIKit
import AVFoundation

//AVFoundation needs API 10.8+. Another alternative is AudioToolbox/CoreAudio/AudioUnit
//Link (https://stackoverflow.com/questions/1877410/whats-the-difference-between-all-these-audio-frameworks)

class ViewController: UIViewController, AVAudioPlayerDelegate {
    
    var audioPlayer : AVAudioPlayer!
    
    let musicArray = ["note1", "note2", "note3", "note4", "note5", "note6", "note7"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }


    @IBAction func notePressed(_ sender: UIButton) {
        
        //when button clicks, play note with tag as the input
        playMusic(sender.tag)
        
    }
    
    private func playMusic(_ tag: Int) {
        
        let soundUrl = Bundle.main.url(forResource: musicArray[tag - 1], withExtension: "wav")
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: soundUrl!)
        } catch {
            print(error)
        }
        
        audioPlayer.play()
        
        //this code below works when importing AudioToolbox
//        if let soundURL = Bundle.main.url(forResource: musicArray[tag - 1], withExtension: "wav") {
//            var mySound: SystemSoundID = 0
//            AudioServicesCreateSystemSoundID(soundURL as CFURL, &mySound)
//            // Play
//            AudioServicesPlaySystemSound(mySound);
//        }
    }
  

}

