//: Playground - noun: a place where people can play

import UIKit

func simpleCalc (number1: Int, number2: Int, operation: (Int, Int) -> Int) -> Int {
    
    return operation(number1, number2)
    
}

//in order to create closure
//1. remove func and its name keyword
//2. put open bracket '{' at the beginning of closure


func multiple (number1: Int, number2: Int) -> Int {
    return number1 * number2
}

func add (number1: Int, number2: Int) -> Int {
    return number1 + number2
}

simpleCalc(number1: 5, number2: 5, operation: { (no1, no2) in
    return no1 * no2
})

//the code above is written the same as
//simpleCalc(number1: 5, number2: 5, operation: multiple)

//Closures are self-contained blocks of functionality that can be passed around and used in your code
//alternative ways in creating closure
//let result = simpleCalc(number1: 5, number2: 5) {$0 * $1}) <- trailing closure
//print(result)
//})

//simpleCalc(number1: 5, number2: 5, operation: { (number1: Int, number2: Int) -> Int in
//    return number1 * number2
//})


let array = [6,3,2,1,4]

let newArray = array.map{"\($0)"}

//array.map({(n1: Int) -> Int in
//    return n1 + 1
//})
