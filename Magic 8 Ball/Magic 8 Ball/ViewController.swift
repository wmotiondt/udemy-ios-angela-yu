//
//  ViewController.swift
//  Magic 8 Ball
//
//  Created by Wmotion Mac 101 on 5/7/18.
//  Copyright © 2018 Wmotion Mac 101. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let ballArray = ["ball1", "ball2", "ball3", "ball4", "ball5"]
    var ballArrayIndex = 0
    
    @IBOutlet weak var ballImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        changeBallView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func askButtonPressed(_ sender: Any) {
        changeBallView()
    }
    
    private func changeBallView() {
        ballArrayIndex = Int(arc4random_uniform(5))
        ballImageView.image = UIImage(named: ballArray[ballArrayIndex])
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        changeBallView()
    }
    
}

